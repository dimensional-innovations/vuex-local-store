import range from "lodash.range";

/**
 * Removes keys from localStorage based on the provided pattern. This can be used
 * to clean up data leftover from previous versions when using the LocalStore.
 * 
 * @param keyPattern - A RegExp to filter the keys on. For example, new RegExp(`^${packageName}:`)
 * @param exclude - An array of keys that shouldn't be removed, even if they match the provided pattern.
 *                  This can be set to the LocalStores active cacheKey.
 */
export function cleanLocalStorage(
  keyPattern: RegExp,
  exclude: Array<string>
): void {
  try {
    const keysToRemove = range(0, localStorage.length)
      .map((i) => localStorage.key(i))
      .filter((key: string | null): key is string => !!key)
      .filter((key) => keyPattern.test(key) && !exclude.includes(key));
    for (const key of keysToRemove) {
      console.log(`Removing localStorage item "${key}"`);
      localStorage.removeItem(key);
    }
  } catch (error) {
    console.warn(error);
  }
}
