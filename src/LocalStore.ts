import merge from 'lodash.merge';
import pick from 'lodash.pick';
import { Store, StoreOptions } from 'vuex';

/**
 * Options used to construct the store instance.
 */
 export interface LocalStoreOptions<T> extends StoreOptions<T> {
  /**
   * The key used to store the state in the cache. This should change
   * if there are any changes to the store to avoid loading an invalid
   * state.
   */
  cacheKey: string;

  /**
   * The paths in the state to include in the cache. If this array is empty,
   * no data will be cached.
   */
  cachePaths: Array<string>;
}

/**
 * Custom store that saves the state to LocalStorage when there is a change.
 */
 export class LocalStore<T> extends Store<T> {

  public readonly cacheKey: string;
  private readonly cachePaths: Array<string>;

  constructor(options: LocalStoreOptions<T>) {
    super(options);

    this.cacheKey = options.cacheKey;
    this.cachePaths = options.cachePaths;

    this.replaceState(merge(this.state, this.load()));
    this.subscribe((_, state) => this.save(state));
  }

  private load(): Partial<T> {
    try {
      const cache = localStorage.getItem(this.cacheKey);
      return !!cache ? JSON.parse(cache) : {};
    }
    catch (e) {
      console.warn(e);
      return {};
    }
  }

  private save(state: T): void {
    try {
      const cache = pick(state, this.cachePaths);
      localStorage.setItem(this.cacheKey, JSON.stringify(cache));
    }
    catch (e) {
      console.warn(e);
    }
  }
}

/**
 * Creates a new instance of LocalStore.
 * 
 * @param options - The options used to create the new instance.
 * @returns - The LocalStore instance.
 */
export function createStore<T>(options: LocalStoreOptions<T>): LocalStore<T> {
  return new LocalStore(options);
}