# @dimensional-innovations/vuex-local-store

A replacement for vuex that syncs the store's state with the brower's LocalStorage. When the application is refreshed, the state is automatically restored and any time a change is made to the state, it's saved to LocalStorage.

## Install

NPM:
```
npm install @dimensional-innovations/vuex-local-store
```

Yarn:
```
yarn add @dimensional-innovations/vuex-local-store
```

## Usage

vuex-local-store is intended to be a drop in replacement for vuex. As such, it uses the same api as vuex to setup a store.

store/index.js
```js
import { name, version } from '../../package.json'

export default createStore({
  cacheKey: `${name}:${version}`
  cachePaths: [
    'data'
  ]
  state: {
    data: {
      ...
    }
  },
  mutations: {},
  actions: {},
  modules: {},
});
```

## Options

Since vuex-local-store is a drop in replacement for a vuex store, all options for vuex are valid options for vuex-local-store. Two additional options are required.

#### cacheKey
The unique key to use to put the state into local storage. A recommend key is `${name}:${version}` in order to use a new cache each with each version.

#### cachePaths
An array of paths to cache. This is used to filter the state down to just what is needed to the cached.

## Cleaning local storage

If we use the cacheKey in the example above, every time we publish a new version of our app, a
new key will be created in the store. This helps to avoid loading an invalid state into the
store, but can bloat the size LocalStorage over time. To help with that, this package exports
cleanLocalStorage, which can be called as your application starts up to remove previous keys.
For example:

main.js
```js
import { cleanLocalStorage } from '@dimensional-innovations/vuex-local-storage';
import { name } from '../package.json';
import store from './store';

cleanLocalStorage(new RegExp(`^${}`), [store.cacheKey]);
```

cleanLocalStorage accepts two arguments:

pattern - A regular expression that is used to determine which keys to remove from local storage. This ensures that we only remove keys related to vuex-local-store.

excludes - An array of keys to avoid removing, even if they match the pattern. At a minimum, the current cacheKey should be included in this array.